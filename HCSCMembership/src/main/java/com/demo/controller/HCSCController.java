package com.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.hcscmembership.HCSCService;

@RestController
@RequestMapping("hcsc")
public class HCSCController {

	
	@Autowired
	HCSCService hcscService;

	@GetMapping(value = "/persist")
	public String call() {
		System.out.println("hello");
		hcscService.persist();

		return "Record persisted Successfully";

	}
}
