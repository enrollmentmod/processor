package com.demo.hcscmembership;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HCSCService {

	
	@Autowired
	HCSCRepo hcscRepo;
	
	public void persist() {
		Member member=new Member();
		member.setFirstName("Abhishek");
		member.setLastName("Singh");
		hcscRepo.save(member);
	}
}
