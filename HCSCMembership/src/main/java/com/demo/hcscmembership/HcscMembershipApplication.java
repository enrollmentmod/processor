package com.demo.hcscmembership;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = {"com.*","com.demo.repo"})
public class HcscMembershipApplication {

	public static void main(String[] args) {
		SpringApplication.run(HcscMembershipApplication.class, args);
	}

}

