package com.demo.hcscmembership;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HCSCRepo extends JpaRepository	<Member, Long>{ 

}
